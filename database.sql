-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 05, 2020 at 11:02 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `examenweb`
--

-- --------------------------------------------------------

--
-- Table structure for table `ex05022020`
--

CREATE TABLE `ex05022020` (
  `nume` varchar(250) NOT NULL,
  `categorie` varchar(250) NOT NULL,
  `imagine` varchar(250) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ex05022020`
--

INSERT INTO `ex05022020` (`nume`, `categorie`, `imagine`, `id`) VALUES
('Nume1', 'categ1', './img/img1.jpg', 1),
('Nume2', 'categ2', './img/img2.jpg', 2),
('nume3', 'categ3', './img/img3.jpg', 3),
('nume4', 'categ4', './img/img4.jpg', 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ex05022020`
--
ALTER TABLE `ex05022020`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ex05022020`
--
ALTER TABLE `ex05022020`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
