<!doctype html>
<html>
  <head>
   
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
  integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
 
 
<link rel="Shortcu Icon" href="" />
 

<title>Titlu</title>
  </head>
  <body>
   
    
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand" href="#"><img class="logo" src="./img/logo.png" alt=""> </a>

      
     
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
    
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Pages
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#">About Us</a>
              <a class="dropdown-item" href="#">Menu</a>
              <a class="dropdown-item" href="#">News & Events</a>
              <a class="dropdown-item" href="#">Chefs</a>
              <a class="dropdown-item" href="#">Coming soon</a>
              <a class="dropdown-item" href="#">Page 404</a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Portofolio
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#">Portofolio 2 Columns</a>
              <a class="dropdown-item" href="#">Portofolio 3 Columns</a>
              <a class="dropdown-item" href="#">Portofolio 4 Columns</a>
            </div>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Blog
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#">BLOG WITH RIGHT SIDEBAR </a>
              <a class="dropdown-item" href="#">BLOG WITH LEFT SIDEBAR</a>
              <a class="dropdown-item" href="#">ARTICLE CATEGORY BLOG</a>
            </div>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              POST FORMATS
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#">STANDARD POST FORMAT </a>
              <a class="dropdown-item" href="#">VIDEO POST FORMAT</a>
              <a class="dropdown-item" href="#"> AUDIO POST FORMAT</a>
              <a class="dropdown-item" href="#"> GALLERY POST FORMAT</a>
              <a class="dropdown-item" href="#"> LINK POST FORMAT</a>
            </div>
          </li>
          

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
             JO0MLA!
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#">CONTENT COMPONENT </a>
              <a class="dropdown-item" href="#">CONTACT COMPONENT</a>
              <a class="dropdown-item" href="#">USER COMPONENT</a>
              <a class="dropdown-item" href="#"> OTHER COMPONENT</a>
            </div>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
             K2 BLOG
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#">BLOG LISTING </a>
              <a class="dropdown-item" href="#">SINGLE BLOG</a>
              <a class="dropdown-item" href="#">USER COMPONENT</a>
              <a class="dropdown-item" href="#"> USER'S BLOG</a>
            </div>
          </li>

        </ul>
        
      </div>
    </nav>

    <section>
    <div class="container">
      <div class="row">
        <div class="col-md-6 m-auto text-center">
          <i class="welcome"></i><br>
          <h4 id="1">WELCOME TO   <a href="" class="restaurant">AT RESTAURANT</a></h4>
        </div>
        </div>

        <div class="row">
          <div class="col-md-8 m-auto text-center">

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
              ut enim ad minim veniam, quis nostrud exercitation ullamco.</p> <BR><BR>
          </div>
          </div>

          <div class="row">
            <div class="col-md-3 mb-3 text-center">
              <i class="fas fa-utensils"></i>
              <h4>BEST CUISINE</h4>
              <p1> Lorem ipsum dolor sit amet, consectetur adipiscing elit.ullamcorper diam nec augue semper, in dignissim. </p1>
            </div>
 
              <div class="col-md-3 mb-3 text-center">
                <i class="fas fa-glass-martini-alt"></i>
                <h4>SPECIAL OFFERS</h4>
                <p1> Lorem ipsum dolor sit amet, consectetur adipiscing elit.ullamcorper diam nec augue semper, in dignissim. </p1>
              </div>
      
                <div class="col-md-3 mb-3 text-center">
                  <i class="fas fa-beer"></i>
                  <h4>GOOD REST</h4>
                  <p1> Lorem ipsum dolor sit amet, consectetur adipiscing elit.ullamcorper diam nec augue semper, in dignissim. </p1>
                </div>
       
                  <div class="col-md-3 mb-3 text-center">
                    <i class="fas fa-glass-martini-alt"></i>
                    <h4>TIMINGS</h4>
                    <p1> Lorem ipsum dolor sit amet, consectetur adipiscing elit.ullamcorper diam nec augue semper, in dignissim. </p1>
                  </div>
          </div>
        </section>

        <section id="latest">
          <div class="row">
            <div class="col-md-6 m-auto text-center">
              <i class="welcome"></i><br>
              <h4 >LATEST WORK </h4>
            </div>
            </div>

            <div class="row">
              <div class="col-md-6 m-auto text-center">
    
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                  ut enim ad minim veniam, quis nostrud exercitation ullamco.</p> 
              </div>
              </div>
              <ul class="nav nav-tabs nav-justified">
  <li class="active"><a href="#">All</a></li>
  <li><a href="#">Apetizers & Soup</a></li>
  <li><a href="#">Souces & Sider</a></li>
  <li><a href="#">Desserts</a></li>
</ul>

<!-- Centered Pills -->
<ul class="nav nav-pills nav-justified">
  <li class="active"><a href="#">Home</a></li>
  <li><a href="#">Menu 1</a></li>
  <li><a href="#">Menu 2</a></li>
  <li><a href="#">Menu 3</a></li>
</ul>
            
<div class="container   ">
              <div class="row ">
                <div class="card" style="width: 15rem;">
                  <img class="card-img-top" src="./img/img1.jpg" alt="Card image cap">
                  <div class="card-body">
                    <p class="nume">nume1</p>
                    <p class="categorie">categ1</p>
                  </div>
                  
                </div>
                <div class="card" style="width: 15rem;">
                  <img class="card-img-top" src="./img/img2.jpg" alt="Card image cap">
                  <div class="card-body">
                    <p class="nume">nume2</p>
                    <p class="categorie">categ2</p>
                  </div>
                </div>
                <div class="card" style="width: 15rem;">
                  <img class="card-img-top" src="./img/img3.jpg" alt="Card image cap">
                  <div class="card-body">
                    <p class="nume">nume3</p>
                    <p class="categorie">categ3</p>
                  </div>
                </div>
                <div class="card" style="width: 15rem;">
                  <img class="card-img-top" src="./img/img4.jpg" alt="Card image cap">
                  <div class="card-body">
                    <p class="nume">nume4</p>
                    <p class="categorie">categ4</p>
                  </div>
                </div>
              </div>

              <div class="row ">
                <div class="card" style="width: 15rem;">
                  <img class="card-img-top" src="./img/5.jpg" alt="Card image cap">
                  <div class="card-body">
                    <p class="nume">nume5</p>
                    <p class="categorie">categ5</p>
                  </div>
                </div>
                <div class="card" style="width: 15rem;">
                  <img class="card-img-top" src="./img/6.jpg" alt="Card image cap">
                  <div class="card-body">
                    <p class="nume">nume6</p>
                    <p class="categorie">categ6</p>
                  </div>
                </div>
                <div class="card" style="width: 15rem;">
                  <img class="card-img-top" src="./img/7.jpg" alt="Card image cap">
                  <div class="card-body">
                    <p class="nume">nume7</p>
                    <p class="categorie">categ7</p>
                  </div>
                </div>
                <div class="card" style="width: 15rem;">
                  <img class="card-img-top" src="./img/8.jpg" alt="Card image cap">
                  <div class="card-body">
                    <p class="nume">nume8</p>
                    <p class="categorie">categ8</p>
                  </div>
                </div>
              </div>
            </div>

        </section>
        <?php
  $servername = "localhost";
  $username = "root";
  $password = "";
  $dbname = "examenweb";
  
  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  }
  
  $sql = "SELECT * FROM `ex05022020`" ;
  $result = $conn->query($sql);
  

  if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        ?>
<div class="container">
<div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="post-preview">
     <p><?php echo $row["nume"]; ?> </p>
     <p><?php echo $row["categorie"] ; ?> </p>
     <p><?php echo $row["imagine"]; ?> </p>
        </div>
     </div>
     
 <?php }
} ?>


    
 
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
  </script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
  </script>
  <script src="https://kit.fontawesome.com/afae9dfecb.js" crossorigin="anonymous"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 
  <script src="bundle.js"></script>
  
  </body>
</html>
